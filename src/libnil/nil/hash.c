/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <nil/hash.h>

#define ROTATE(x, r)    ((x << r) | (x >> (64 - r)))
static uint64_t final_mix(uint64_t);

/* A hash function. */
uint64_t
nil_murmur_hash(void const *key, size_t length, uint32_t seed)
{
	uint8_t const *data = key;
	uint64_t const *blocks = key;
	size_t const num_blocks = length / 16;

	uint64_t h1 = seed;
	uint64_t h2 = seed;
	uint64_t k1;
	uint64_t k2;

	uint64_t const c1 = 0x87c37b91114253d5ULL;
	uint64_t const c2 = 0x4cf5ad432745937fULL;

	for (size_t i = 0; i < num_blocks; ++i) {
		k1 = blocks[i*2];
		k2 = blocks[i*2 + 1];

		k1 *= c1;
		k1 = ROTATE(k1, 31);
		k1 *= c2;

		h1 ^= k1;
		h1 = ROTATE(h1, 27);
		h1 += h2;
		h1 = h1*5 + 0x52dce729;

		k2 *= c2;
		k2 = ROTATE(k2, 33);
		k2 *= c1;

		h2 ^= k2;
		h2 = ROTATE(h2, 31);
		h2 += h1;
		h2 = h2*5 + 0x38495ab5;
	}

	uint8_t const *tail = &data[num_blocks * 16];
	k1 = 0;
	k2 = 0;

	switch (length % 16) {
	case 15:
		k2 ^= ((uint64_t) tail[14]) << 48;
		/* FALLTHROUGH */
	case 14:
		k2 ^= ((uint64_t) tail[13]) << 40;
		/* FALLTHROUGH */
	case 13:
		k2 ^= ((uint64_t) tail[12]) << 32;
		/* FALLTHROUGH */
	case 12:
		k2 ^= ((uint64_t) tail[11]) << 24;
		/* FALLTHROUGH */
	case 11:
		k2 ^= ((uint64_t) tail[10]) << 16;
		/* FALLTHROUGH */
	case 10:
		k2 ^= ((uint64_t) tail[9]) << 8;
		/* FALLTHROUGH */
	case 9:
		k2 ^= ((uint64_t) tail[8]) << 0;
		k2 *= c2;
		k2 = ROTATE(k2, 33);
		k2 *= c1;
		h2 ^= k2;
		/* FALLTHROUGH */
	case 8:
		k1 ^= ((uint64_t)tail[7]) << 56;
		/* FALLTHROUGH */
	case 7:
		k1 ^= ((uint64_t)tail[6]) << 48;
		/* FALLTHROUGH */
	case 6:
		k1 ^= ((uint64_t)tail[5]) << 40;
		/* FALLTHROUGH */
	case 5:
		k1 ^= ((uint64_t)tail[4]) << 32;
		/* FALLTHROUGH */
	case 4:
		k1 ^= ((uint64_t)tail[3]) << 24;
		/* FALLTHROUGH */
	case 3:
		k1 ^= ((uint64_t)tail[2]) << 16;
		/* FALLTHROUGH */
	case 2:
		k1 ^= ((uint64_t)tail[1]) << 8;
		/* FALLTHROUGH */
	case 1:
		k1 ^= ((uint64_t)tail[0]) << 0;
		k1 *= c1;
		k1 = ROTATE(k1, 31);
		k1 *= c2;
		h1 ^= k1;
	}

	h1 ^= length;
	h2 ^= length;

	h1 += h2;
	h2 += h1;

	h1 = final_mix(h1);
	h2 = final_mix(h2);

	h1 += h2;
	h2 += h1;

	return h1 ^ h2;
}

uint64_t
final_mix(uint64_t k)
{
	k ^= k >> 33;
	k *= 0xff51afd7ed558ccdULL;
	k ^= k >> 33;
	k *= 0xc4ceb9fe1a85ec53ULL;
	k ^= k >> 33;

	return k;
}

uint64_t
nil_murmur_hash_string(char const *string,
                       uint32_t seed)
{
	return nil_murmur_hash(string, strlen(string), seed);
}
