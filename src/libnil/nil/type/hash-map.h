/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

typedef struct NilHashMap NilHashMap;
typedef bool (*NilHashMapFindFunc)(void *, void *, void *);

NilHashMap     *nil_hash_map_make(NilComparable *);

bool            nil_hash_map_p(void const *);
bool            nil_hash_map_contains_p(NilHashMap const *,
                                        void const *);
bool            nil_hash_map_empty_p(NilHashMap const *);
bool            nil_hash_map_equal_p(NilHashMap const *,
                                     NilHashMap const *);

void           *nil_hash_map_ref(NilHashMap const *,
                                 void const *);
void           *nil_hash_map_ref_default(NilHashMap const *,
                                         void const *,
                                         void *);

void            nil_hash_map_set(NilHashMap *,
                                 void *,
                                 void *);
bool            nil_hash_map_delete(NilHashMap *,
                                    void *);
void           *nil_hash_map_pop(NilHashMap *,
                                 void **);
void            nil_hash_map_clear(NilHashMap *);

size_t          nil_hash_map_size(NilHashMap const *);
NilList        *nil_hash_map_keys(NilHashMap const *);
NilList        *nil_hash_map_values(NilHashMap const *);
bool            nil_hash_map_find(NilFunction *,
                                  NilHashMap const *);
size_t          nil_hash_map_count(NilFunction *,
                                   NilHashMap const *);
