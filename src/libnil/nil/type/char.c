/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <nil/object.h>
#include <nil/type/char.h>

struct NilChar {
	NilTerm         term;
	uint32_t        char_;
};

static NilType const char_type = {
	.free         = nil_free_generic,
	.size         = sizeof(NilChar),
	.children     = nil_no_children,
	.flags        = nil_type_flags_none,
};

NilChar *
nil_char_make(uint32_t c) {
	NilChar *term = nil_make(&char_type);
	if (term == NULL) {
		return NULL;
	}
	term->char_ = c;
	return term;
}

bool
nil_char_p(void const *term) {
	return nil_type_p(&char_type, term);
}

uint32_t
nil_char_value(NilChar *term) {
	return term->char_;
}
