/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <nil/object.h>
#include <nil/type/pair.h>

struct NilPair {
	NilTerm         term;
	void           *first;
	void           *second;
};

static void   **pair_children(NilPair const *,
                              size_t *);

static NilType const pair_type = {
	.free         = nil_free_generic,
	.size         = sizeof(NilPair),
	.children     = (NilChildrenFunc) pair_children,
	.flags        = nil_type_flags_can_cycle,
};

NilPair *
nil_pair_make(void *first, void *second)
{
	NilPair *pair = nil_make(&pair_type);
	pair->first = nil_keep(first);
	pair->second = nil_keep(second);
	return pair;
}

bool
nil_pair_p(void const *term)
{
	return nil_type_p(&pair_type, term);
}

void *
nil_pair_first(NilPair *pair) {
	return nil_keep(pair->first);
}

void *
nil_pair_second(NilPair *pair) {
	return nil_keep(pair->second);
}

void **
pair_children(NilPair const *pair,
              size_t        *num_children)
{
	void **children = malloc(sizeof(*children) * 2);
	children[0] = pair->first;
	children[1] = pair->second;
	*num_children = 2;
	return children;
}
