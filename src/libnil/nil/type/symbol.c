/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <nil/object.h>
#include <nil/type/symbol.h>

struct NilSymbol {
	NilTerm         term;
	char const     *symbol;
	size_t          length;
	bool            static_;
};

static void     symbol_free(NilSymbol *);

static NilType const symbol_type = {
	.free         = (NilFreeFunc) symbol_free,
	.size         = sizeof(NilSymbol),
	.children     = nil_no_children,
	.flags        = nil_type_flags_none,
};

NilSymbol *
nil_symbol_make(char const *symbol,
                size_t length)
{
	NilSymbol *term = nil_make(&symbol_type);
	term->symbol = symbol;
	term->length = length;
	term->static_ = false;
	return term;
}

NilSymbol *
nil_symbol_make_static(char const *symbol)
{
	size_t length = strlen(symbol);
	NilSymbol *term = nil_symbol_make(symbol, length);
	term->static_ = true;
	return term;
}

bool
nil_symbol_p(void const *term)
{
	return nil_type_p(&symbol_type, term);
}

char const *
nil_symbol_value(NilSymbol const *term)
{
	return term->symbol;
}

size_t
nil_symbol_length(NilSymbol const *term)
{
	return term->length;
}

void
symbol_free(NilSymbol *term)
{
	if (!term->static_) {
		nil_free_generic((void *) term->symbol);
	}
	nil_free_generic(term);
}
