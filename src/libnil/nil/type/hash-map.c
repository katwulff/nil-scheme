/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <nil/object.h>
#include <nil/class/hashable.h>
#include <nil/class/equal.h>
#include <nil/class/ordered.h>
#include <nil/class/comparable.h>
#include <nil/type/list.h>
#include <nil/type/function.h>
#include <nil/type/hash-map.h>

#define DEFAULT_SIZE 8
#define BUCKET_DELETED_SENTINEL ((void *) -1)

typedef struct Bucket {
	void           *key;
	void           *value;
	uint64_t        hash;
} Bucket;

struct NilHashMap {
	NilTerm         term;
	size_t          size;
	size_t          capacity;
	Bucket         *buckets;
	NilComparable  *comparable;
};

typedef bool (*BucketPred)(NilHashMap const *,
                           void const        *,
                           uint64_t,
                           Bucket const      *);

static void   **hash_map_children(NilHashMap const *,
                                  size_t           *);
static void     hash_map_free(NilHashMap *);

static NilType const hash_map_type = {
	.free         = (NilFreeFunc) hash_map_free,
	.size         = sizeof(NilHashMap),
	.children     = (NilChildrenFunc) hash_map_children,
	.flags        = nil_type_flags_can_cycle,
};

static Bucket  *bucket_at(NilHashMap const *,
                          uint64_t);
static void     bucket_delete(Bucket *);
static bool     bucket_deleted(Bucket const *);
static bool     bucket_empty(Bucket const *);
static bool     bucket_found(NilHashMap const *,
                             void const *,
                             uint64_t,
                             Bucket const *);
static bool     bucket_occupied(Bucket const *);
static bool     bucket_unoccupied(Bucket const *);
static void     decrease_size(NilHashMap *);
static void     decrement_size(NilHashMap *);
static Bucket  *find_bucket(NilHashMap const *,
                            void const *,
                            uint64_t,
                            BucketPred);
static Bucket  *find_key(NilHashMap const *,
                            void const *,
                            uint64_t);
static Bucket  *find_unoccupied(NilHashMap const *,
                                uint64_t);
static uint64_t get_hash(NilHashMap const *,
                         void const *);
static void     increase_size(NilHashMap *);
static void     increment_size(NilHashMap *);
static bool     key_equal_p(NilHashMap const *,
                            void const *,
                            void const *);
static bool     matches_key(NilHashMap const *,
                            void const *,
                            uint64_t,
                            Bucket const *);
static void     reinsert_bucket(NilHashMap *,
                                Bucket     *);
static void     reset(NilHashMap *);
static void     resize(NilHashMap *,
                       size_t);
static bool     too_empty(NilHashMap const *);
static bool     too_full(NilHashMap const *);
static bool     unoccupied_p(NilHashMap const *,
                             void const *,
                             uint64_t,
                             Bucket const *);

NilHashMap *
nil_hash_map_make(NilComparable *comparable)
{
	NilHashMap *hash_map = nil_make(&hash_map_type);
	reset(hash_map);
	hash_map->comparable = nil_keep(comparable);
	return hash_map;
}

void
reset(NilHashMap *hash_map)
{
	hash_map->size = 0;
	hash_map->capacity = DEFAULT_SIZE;
	hash_map->buckets = calloc(DEFAULT_SIZE, sizeof(Bucket));
}

bool
nil_hash_map_p(void const *term)
{
	return nil_type_p(&hash_map_type, term);
}

bool
nil_hash_map_contains_p(NilHashMap const *hash_map,
                       void const        *key)
{
	void *value = nil_hash_map_ref(hash_map, key);
	if (value == NULL) {
		return false;
	} else {
		nil_free(value);
		return true;
	}
}

bool
nil_hash_map_empty_p(NilHashMap const *hash_map)
{
	return hash_map->size == 0;
}

/* TODO
 * Hashmaps should be equal if they contain the same key/value pairs,
 * not just if they're in the same buckets.
 */
bool
nil_hash_map_equal_p(NilHashMap const *a,
                     NilHashMap const *b)
{
	(void) a;
	(void) b;
	return false;
}

void *
nil_hash_map_ref(NilHashMap const *hash_map,
                 void const       *key)
{
	return nil_hash_map_ref_default(hash_map, key, NULL);
}

void *
nil_hash_map_ref_default(NilHashMap const *hash_map,
                         void const       *key,
                         void             *default_)
{
	uint64_t hash = get_hash(hash_map, key);
	Bucket *bucket = find_key(hash_map, key, hash);
	if (bucket->key == NULL) {
		return nil_keep(default_);
	} else {
		return nil_keep(bucket->value);
	}
}

uint64_t
get_hash(NilHashMap const *hash_map,
         void const *key)
{
	return nil_hash(hash_map->comparable->hashable, key);
}

Bucket *
find_key(NilHashMap const *hash_map,
            void const    *key,
            uint64_t       hash)
{
	return find_bucket(hash_map, key, hash, matches_key);
}

bool
matches_key(NilHashMap const *hash_map,
            void const       *key,
            uint64_t          hash,
            Bucket const     *bucket)
{
	return bucket_empty(bucket) ||
               bucket_found(hash_map, key, hash, bucket);
}

Bucket *
find_bucket(NilHashMap const *hash_map,
            void const       *key,
            uint64_t          hash,
            BucketPred        pred)
{
	uint64_t index = hash;
	uint64_t skip = 1;
	while (true) {
		Bucket *bucket = bucket_at(hash_map, index);
		if (pred(hash_map, key, hash, bucket)) {
			return bucket;
		}
		index += skip;
		skip += 1;
	}
}

Bucket *
bucket_at(NilHashMap const *hash_map,
          uint64_t hash)
{
	return &hash_map->buckets[hash & (hash_map->capacity - 1)];
}

bool
bucket_empty(Bucket const *bucket)
{
	return bucket->key == NULL;
}

bool
bucket_found(NilHashMap const *hash_map,
             void const       *key,
             uint64_t          hash,
             Bucket const     *bucket)
{
	return (bucket->hash == hash) &&
	       bucket_occupied(bucket) &&
	       (key_equal_p(hash_map, key, bucket->key));
}

bool
key_equal_p(NilHashMap const *hash_map,
            void const       *key_a,
            void const       *key_b)
{
	return nil_equal_p(hash_map->comparable->ordered->equal,
	                   key_a,
	                   key_b);
}

bool
bucket_deleted(Bucket const *bucket)
{
	return bucket->key == BUCKET_DELETED_SENTINEL;
}

void
nil_hash_map_set(NilHashMap *hash_map, void *key, void *value)
{
	uint64_t hash = get_hash(hash_map, key);
	Bucket *bucket = find_key(hash_map, key, hash);
	if (bucket->key == NULL) {
		bucket = find_unoccupied(hash_map, hash);
		bucket->key = nil_keep(key);
		bucket->hash = hash;
		bucket->value = nil_keep(value);
		increment_size(hash_map);
	} else {
		nil_free(bucket->value);
		bucket->value = nil_keep(value);
	}
}

Bucket *
find_unoccupied(NilHashMap const *hash_map,
                uint64_t hash)
{
	return find_bucket(hash_map, NULL, hash, unoccupied_p);
}

bool
unoccupied_p(NilHashMap const *hash_map,
             void const       *key,
             uint64_t          hash,
             Bucket const     *bucket)
{
	(void) hash_map;
	(void) key;
	(void) hash;
	return bucket_unoccupied(bucket);
}

void
increment_size(NilHashMap *hash_map)
{
	hash_map->size += 1;
	if (too_full(hash_map)) {
		increase_size(hash_map);
	}
}

void
decrement_size(NilHashMap *hash_map)
{
	hash_map->size -= 1;
	if (too_empty(hash_map)) {
		decrease_size(hash_map);
	}
}

bool
too_full(NilHashMap const *hash_map)
{
	return hash_map->size >= ((hash_map->capacity * 3) / 4);
}

bool
too_empty(NilHashMap const *hash_map)
{
	return (hash_map->size <= (hash_map->capacity / 4)) &&
	       (hash_map->capacity > DEFAULT_SIZE);
}

void
increase_size(NilHashMap *hash_map)
{
	size_t new_capacity = hash_map->capacity * 2;
	resize(hash_map, new_capacity);
}

void
decrease_size(NilHashMap *hash_map)
{
	size_t new_capacity = hash_map->capacity / 2;
	resize(hash_map, new_capacity);
}

void
resize(NilHashMap *hash_map,
       size_t new_capacity)
{
	Bucket *new_buckets = calloc(new_capacity, sizeof(Bucket));
	Bucket *old_buckets = hash_map->buckets;
	size_t old_capacity = hash_map->capacity;
	hash_map->buckets = new_buckets;
	hash_map->capacity = new_capacity;
	for (size_t i = 0; i < old_capacity; ++i) {
		Bucket *bucket = &old_buckets[i];
		reinsert_bucket(hash_map, bucket);
	}
	nil_free_generic(old_buckets);
}

void
reinsert_bucket(NilHashMap *hash_map,
                Bucket     *bucket)
{
	if (bucket_occupied(bucket)) {
		void *key = bucket->key;
		void *value = bucket->value;
		nil_hash_map_set(hash_map, key, value);
		nil_free(key);
		nil_free(value);
	}
}

bool
bucket_occupied(Bucket const *bucket)
{
	return !bucket_unoccupied(bucket);
}

bool
bucket_unoccupied(Bucket const *bucket)
{
	return bucket_empty(bucket) ||
	       bucket_deleted(bucket);
}

bool
nil_hash_map_delete(NilHashMap *hash_map,
                    void       *key)
{
	uint64_t hash = get_hash(hash_map, key);
	Bucket *bucket = find_key(hash_map, key, hash);
	if (bucket->key == NULL) {
		return false;
	} else {
		bucket_delete(bucket);
		decrement_size(hash_map);
		return true;
	}
}

void
bucket_delete(Bucket *bucket)
{
	nil_free(bucket->key);
	nil_free(bucket->value);
	bucket->key = BUCKET_DELETED_SENTINEL;
}

void *
nil_hash_map_pop(NilHashMap  *hash_map,
                 void       **key)
{
	for (size_t i = 0; i < hash_map->capacity; ++i) {
		Bucket *bucket = &hash_map->buckets[i];
		if (bucket_occupied(bucket)) {
			*key = nil_keep(bucket->key);
			void *value = nil_keep(bucket->value);
			bucket_delete(bucket);
			decrement_size(hash_map);
			return value;
		}
	}
	*key = NULL;
	return NULL;
}

void
nil_hash_map_clear(NilHashMap *hash_map)
{
	for (size_t i = 0; i < hash_map->capacity; ++i) {
		Bucket *bucket = &hash_map->buckets[i];
		if (bucket_occupied(bucket)) {
			bucket_delete(bucket);
		}
	}
	nil_free_generic(hash_map->buckets);
	reset(hash_map);
}

size_t
nil_hash_map_size(NilHashMap const *hash_map)
{
	return hash_map->size;
}

NilList *
nil_hash_map_keys(NilHashMap const *hash_map)
{
	NilList *lst = nil_list_empty();
	for (size_t i = 0; i < hash_map->capacity; ++i) {
		Bucket *bucket = &hash_map->buckets[i];
		if (bucket_occupied(bucket)) {
			NilList *new_lst = nil_list_cons(bucket->key,
			                                 lst);
			nil_free(lst);
			lst = new_lst;
		}
	}
	return lst;
}

NilList *
nil_hash_map_values(NilHashMap const *hash_map)
{
	NilList *lst = nil_list_empty();
	for (size_t i = 0; i < hash_map->capacity; ++i) {
		Bucket *bucket = &hash_map->buckets[i];
		if (bucket_occupied(bucket)) {
			NilList *new_lst = nil_list_cons(bucket->value,
			                                 lst);
			nil_free(lst);
			lst = new_lst;
		}
	}
	return lst;
}

bool
nil_hash_map_find(NilFunction      *function,
                  NilHashMap const *hash_map)
{
	void *environment = nil_function_environment(function);
	NilHashMapFindFunc func =
	    (NilHashMapFindFunc) nil_function_func(function);
	bool ret = false;
	for (size_t i = 0; i < hash_map->capacity; ++i) {
		Bucket *bucket = &hash_map->buckets[i];
		if (bucket_occupied(bucket) &&
		    func(environment, bucket->key, bucket->value)) {
			ret = true;
			break;
		}
	}
	nil_free(environment);
	return ret;
}

size_t
nil_hash_map_count(NilFunction      *function,
                  NilHashMap const *hash_map)
{
	void *environment = nil_function_environment(function);
	NilHashMapFindFunc func =
	    (NilHashMapFindFunc) nil_function_func(function);
	size_t counts = 0;
	for (size_t i = 0; i < hash_map->capacity; ++i) {
		Bucket *bucket = &hash_map->buckets[i];
		if (bucket_occupied(bucket) &&
		    func(environment, bucket->key, bucket->value)) {
			counts += 1;
		}
	}
	nil_free(environment);
	return counts;
}

void **
hash_map_children(NilHashMap const *hash_map,
                  size_t           *num_children)
{
	void **children = calloc(hash_map->size, sizeof(*children));
	size_t index = 0;
	for (size_t i = 0; i < hash_map->capacity; ++i) {
		Bucket *bucket = &hash_map->buckets[i];
		if (bucket_occupied(bucket)) {
			children[index] = bucket->key;
			children[index+1] = bucket->value;
			index += 2;
		}
	}
	*num_children = hash_map->size;
	return children;
}

void
hash_map_free(NilHashMap *hash_map)
{
	nil_hash_map_clear(hash_map);
	nil_free(hash_map->comparable);
	nil_free_generic(hash_map->buckets);
	nil_free_generic(hash_map);
}
