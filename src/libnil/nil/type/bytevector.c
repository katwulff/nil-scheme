/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <nil/object.h>
#include <nil/type/bytevector.h>

struct NilBytevector {
	NilTerm         term;
	uint8_t        *bytevector;
	size_t          length;
	bool            static_;
};

static void     bytevector_free(NilBytevector *);

static NilType const bytevector_type = {
	.free         = (NilFreeFunc) bytevector_free,
	.size         = sizeof(NilBytevector),
	.children     = nil_no_children,
	.flags        = nil_type_flags_none,
};

NilBytevector *
nil_bytevector_make(uint8_t *bytevector, size_t length) {
	NilBytevector *term = nil_make(&bytevector_type);
	if (term == NULL) {
		return NULL;
	}
	term->bytevector = bytevector;
	term->length = length;
	term->static_ = false;
	return term;
}

NilBytevector *
nil_bytevector_make_static(uint8_t *bytevector, size_t length) {
	NilBytevector *term = nil_bytevector_make(bytevector, length);
	term->static_ = true;
	return term;
}

bool
nil_bytevector_p(void const *term) {
	return nil_type_p(&bytevector_type, term);
}

uint8_t *
nil_bytevector_value(NilBytevector *bytevector) {
	return bytevector->bytevector;
}

size_t
nil_bytevector_length(NilBytevector *bytevector) {
	return bytevector->length;
}

void
bytevector_free(NilBytevector *bytevector) {
	if (!bytevector->static_) {
		free((void *) bytevector->bytevector);
	}
	nil_free_generic(bytevector);
}
