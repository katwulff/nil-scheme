/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>

#include <nil/object.h>
#include <nil/type/null.h>

struct NilNull {
	NilTerm         term;
};

static NilType const null_type = {
	.free         = nil_free_static,
	.size         = sizeof(NilNull),
	.children     = nil_no_children,
	.flags        = nil_type_flags_none,
};

static NilNull null = {
	.term = {
		.rc           = 1,
		.type         = &null_type,
	},
};

NilNull *
nil_null_make() {
	return nil_keep(&null);
}

bool
nil_null_p(void const *term)
{
	return nil_type_p(&null_type, term);
}
