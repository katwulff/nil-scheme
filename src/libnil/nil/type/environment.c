/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <nil/object.h>
#include <nil/class/hashable.h>
#include <nil/class/equal.h>
#include <nil/class/ordered.h>
#include <nil/class/comparable.h>
#include <nil/type/list.h>
#include <nil/type/function.h>
#include <nil/type/hash-map.h>
#include <nil/type/symbol.h>
#include <nil/class/comparable/symbol-comparable.h>
#include <nil/type/environment.h>

struct NilEnvironment {
	NilTerm         term;
	NilEnvironment *parent;
	NilHashMap     *map;
};

static void   **environment_children(NilEnvironment const *,
                                     size_t               *);

static NilType const environment_type = {
	.free         = nil_free_generic,
	.size         = sizeof(NilEnvironment),
	.children     = (NilChildrenFunc) environment_children,
	.flags        = nil_type_flags_can_cycle,
};

NilEnvironment *
nil_environment_make(NilEnvironment *parent)
{
	NilEnvironment *environment = nil_make(&environment_type);
	environment->parent = nil_keep(parent);
	environment->map = nil_hash_map_make(&nil_symbol_comparable);
	return environment;
}

bool
nil_environment_p(void *term)
{
	return nil_type_p(&environment_type, term);
}

void
nil_environment_define(NilEnvironment *environment,
                       NilSymbol      *symbol,
                       void           *term)
{
	nil_hash_map_set(environment->map, symbol, term);
}

void *
nil_environment_lookup(NilEnvironment const *environment,
                       NilSymbol const      *symbol)
{
	return nil_hash_map_ref(environment->map, symbol);
}

void **
environment_children(NilEnvironment const *environment,
                     size_t               *num_children)
{
	void **children = malloc(sizeof(*children) * 2);
	children[0] = environment->parent;
	children[1] = environment->map;
	*num_children = 2;
	return children;
}
