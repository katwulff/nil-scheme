/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <nil/object.h>
#include <nil/type/string.h>
#include <nil/type/symbol.h>
#include <nil/class/write.h>
#include <nil/class/write/symbol-write.h>

static NilString *symbol_write(NilWrite *,
                               NilSymbol const *);

static NilType symbol_write_type = {
	.free         = nil_free_static,
	.size         = sizeof(NilWrite),
	.children     = nil_no_children,
	.flags        = nil_type_flags_none,
};

NilWrite nil_write_ordered = {
	.term         = {
		.rc           = 1,
		.type         = &symbol_write_type,
	},
	.write        = (NilWriteFunc) symbol_write,
};

NilString *
symbol_write(NilWrite *write,
             NilSymbol const *symbol)
{
	(void) write;
	char const *raw_string = nil_symbol_value(symbol);
	size_t length = nil_symbol_length(symbol);
	char *new_raw_string = malloc(length * sizeof(*new_raw_string));
	memcpy(new_raw_string, raw_string, length);
	return nil_string_make(new_raw_string, length);
}
