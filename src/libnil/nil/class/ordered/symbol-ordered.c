/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <nil/object.h>
#include <nil/type/symbol.h>
#include <nil/class/equal.h>
#include <nil/class/equal/symbol-equal.h>
#include <nil/class/ordered.h>
#include <nil/class/ordered/symbol-ordered.h>

typedef bool LengthCompareFunc(size_t, size_t);

static bool     length_less_than(size_t,
                                 size_t);
static bool     length_less_than_or_equal_to(size_t,
                                             size_t);
static bool     less_than_compare(LengthCompareFunc,
                                  NilSymbol const *,
                                  NilSymbol const *);
static bool     symbol_less_than_p(NilOrdered *,
                                   NilSymbol const *,
                                   NilSymbol const *);
static bool     symbol_greater_than_p(NilOrdered *,
                                      NilSymbol const *,
                                      NilSymbol const *);
static bool     symbol_less_than_or_equal_to_p(NilOrdered *,
                                               NilSymbol const *,
                                               NilSymbol const *);
static bool     symbol_greater_than_or_equal_to_p(NilOrdered *,
                                                  NilSymbol const *,
                                                  NilSymbol const *);

static NilType symbol_ordered_type = {
	.free         = nil_free_static,
	.size         = sizeof(NilOrdered),
	.children     = nil_no_children,
	.flags        = nil_type_flags_none,
};

NilOrdered nil_symbol_ordered = {
	.term         = {
		.rc           = 1,
		.type         = &symbol_ordered_type,
	},
	.equal        = &nil_symbol_equal,
	.less_than_p =
	    (NilLessThanPFunc) symbol_less_than_p,
	.greater_than_p =
	    (NilGreaterThanPFunc) symbol_greater_than_p,
	.less_than_or_equal_to_p =
	    (NilLessThanOrEqualToPFunc) symbol_less_than_or_equal_to_p,
	.greater_than_or_equal_to_p =
	    (NilGreaterThanOrEqualToPFunc)
	    symbol_greater_than_or_equal_to_p,
};

bool
less_than_compare(LengthCompareFunc func,
                  NilSymbol const *symbol_a,
                  NilSymbol const *symbol_b)
{
	size_t symbol_a_length = nil_symbol_length(symbol_a);
	size_t symbol_b_length = nil_symbol_length(symbol_b);
	size_t smaller_length = symbol_a_length < symbol_b_length ?
	                        symbol_a_length : symbol_b_length;
	char const *symbol_a_value = nil_symbol_value(symbol_a);
	char const *symbol_b_value = nil_symbol_value(symbol_b);
	int cmp =
	    strncmp(symbol_a_value, symbol_b_value, smaller_length);
	if (cmp == 0) {
		return func(symbol_a_length, symbol_b_length);
	} else {
		return cmp < 0;
	}

}

bool
symbol_less_than_p(NilOrdered *ordered,
                   NilSymbol const *symbol_a,
                   NilSymbol const *symbol_b)
{
	(void) ordered;
	return less_than_compare(length_less_than, symbol_a, symbol_b);
}

bool
symbol_greater_than_p(NilOrdered *ordered,
                      NilSymbol const *symbol_a,
                      NilSymbol const *symbol_b)
{
	return symbol_less_than_p(ordered, symbol_b, symbol_a);
}

bool
symbol_less_than_or_equal_to_p(NilOrdered *ordered,
                               NilSymbol const *symbol_a,
                               NilSymbol const *symbol_b)
{
	(void) ordered;
	return less_than_compare(length_less_than_or_equal_to,
	                         symbol_a,
	                         symbol_b);
}

bool
symbol_greater_than_or_equal_to_p(NilOrdered *ordered,
                                  NilSymbol const *symbol_a,
                                  NilSymbol const *symbol_b)
{
	return symbol_less_than_or_equal_to_p(ordered,
	                                      symbol_b,
	                                      symbol_a);
}

bool
length_less_than(size_t length_a,
                 size_t length_b)
{
	return length_a < length_b;
}

bool
length_less_than_or_equal_to(size_t length_a,
                             size_t length_b)
{
	return length_a <= length_b;
}
