/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <nil/object.h>

typedef struct Map Map;

struct Map {
	uint64_t        hash;
	NilTerm const  *term;
	Map            *left;
	Map            *right;
	Map           **children;
	size_t          num_children;
	size_t          references;
	bool            visited;
	bool            safe;
	bool            freed;
};

static void     collect_garbage(Map *);
static bool     correct_node(NilTerm const *, uint64_t, Map const *);
static void     count_references(NilTerm const *, Map *);
static void     decrement_rc(NilTerm *);
static void     detect_cycle(NilTerm *);
static Map *    find_node(NilTerm const *, uint64_t, Map *);
static void     free_children(void **, size_t);
static void     free_term(NilTerm *);
static void     free_term_and_children(NilTerm *);
static uint64_t hash(void const *);
static void     increment_rc(NilTerm *);
static void     map_free(Map *);
static Map     *map_make(NilTerm const *, uint64_t);
static void     mark_safe_terms(Map *);
static void     mark_safe_map(Map *);
static bool     map_should_be_freed(Map const *);
static size_t   rc(NilTerm const *);
static bool     term_can_cycle(NilTerm const *);
static bool     type_can_cycle(NilType const *);

void *
nil_make(NilType const *type)
{
	size_t size = nil_type_size(type);
	NilTerm *term = calloc(1, size);
	term->rc = 1;
	term->type = type;
	return term;
}

void *
nil_keep(void *t)
{
	NilTerm *term = t;
	if (term != NULL) {
		increment_rc(term);
	}
	return term;
}

void
nil_free(void *t)
{
	NilTerm *term = t;
	if (term != NULL) {
		if (rc(term) == 1) {
			free_term_and_children(term);
		} else {
			decrement_rc(term);
			if (term_can_cycle(term)) {
				detect_cycle(term);
			}
		}
	}
}

void
free_term_and_children(NilTerm *term)
{
	size_t num_children;
	void *children = nil_children(term, &num_children);
	free_term(term);
	free_children(children, num_children);
}

void
free_term(NilTerm *term)
{
	NilType const *type = nil_type(term);
	NilFreeFunc free = nil_type_free(type);
	if (free != NULL) {
		free(term);
	}
}

void
free_children(void **children, size_t num_children)
{
	for (size_t i = 0; i < num_children; ++i) {
		nil_free(children[i]);
	}
	free(children);
}

bool
term_can_cycle(NilTerm const *term)
{
	if (term == NULL) {
		return false;
	}
	return type_can_cycle(nil_type(term));
}

bool
type_can_cycle(NilType const *type)
{
	if (type == NULL) {
		return false;
	} else {
		return type->flags & nil_type_flags_can_cycle;
	}
}

void
detect_cycle(NilTerm *term)
{
	Map *map = map_make(term, hash(term));
	count_references(term, map);
	mark_safe_terms(map);
	collect_garbage(map);
	map_free(map);
}

Map *
map_make(NilTerm const *term, uint64_t hash)
{
	Map *map = calloc(1, sizeof(*map));
	map->hash = hash;
	map->term = term;
	return map;
}

void
map_free(Map *map)
{
	if (map != NULL) {
		free(map->children);
		Map *left = map->left;
		Map *right = map->right;
		free(map);
		/* We remember the left and right pointers and do this
		 * out of order in order to achieve TCO (if the
		 * compiler supports it).
		 */
		map_free(left);
		map_free(right);
	}
}

uint64_t
hash(void const *pointer)
{
	/* This `hash` algorithm is actually based on xorshift. */
	uint64_t p = (uint64_t) pointer;
	p ^= p >> 12;
	p ^= p << 25;
	p ^= p >> 27;
	return p * 0x2545F4914F6CDD1DULL;
}

Map *
find_node(NilTerm const *term, uint64_t hash, Map *node)
{
	if (correct_node(term, hash, node)) {
		return node;
	} else if (hash <= node->hash) {
		if (node->left == NULL) {
			return node->left = map_make(term, hash);
		} else {
			return find_node(term, hash, node->left);
		}
	} else {
		if (node->right == NULL) {
			return node->right = map_make(term, hash);
		} else {
			return find_node(term, hash, node->right);
		}
	}
}

bool
correct_node(NilTerm const *term, uint64_t hash, Map const *node)
{
	return node->hash == hash &&
	       node->term == term;
}

void
count_references(NilTerm const *term, Map *map)
{
	Map *node = find_node(term, hash(term), map);
	if (!node->visited) {
		node->visited = true;
		size_t num_children;
		void **children = nil_children(term, &num_children);
		node->num_children = num_children;
		node->children = calloc(num_children, sizeof(Map *));
		for (size_t i = 0; i < num_children; ++i) {
			NilTerm const *child = children[i];
			Map *child_node =
			    find_node(child, hash(child), map);
			child_node->references += 1;
			node->children[i] = child_node;
			if (term_can_cycle(child)) {
				count_references(child, map);
			}
		}
	}
}

void
mark_safe_terms(Map *map)
{
	if (map != NULL &&
	    map->term != NULL) {
		if (rc(map->term) > map->references) {
			mark_safe_map(map);
		}
		mark_safe_terms(map->left);
		mark_safe_terms(map->right);
	}
}

void
mark_safe_map(Map *map)
{
	if (!map->safe) {
		map->safe = true;
		if (term_can_cycle(map->term)) {
			size_t num_children = map->num_children;
			Map **children = map->children;
			for (size_t i = 0; i < num_children; ++i) {
				Map *child = children[i];
				mark_safe_map(child);
			}
		}
	}
}

void
collect_garbage(Map *map)
{
	if (map_should_be_freed(map)) {
		map->freed = true;
		size_t num_children = map->num_children;
		Map **children = map->children;
		for (size_t i = 0; i < num_children; ++i) {
			Map *child = children[i];
			collect_garbage(child);
		}
		free_term((NilTerm *) map->term);
	}
}

bool
map_should_be_freed(Map const *map)
{
	return (map != NULL) &&
	       !map->safe &&
	       !map->freed;
}

size_t
rc(NilTerm const *term)
{
	return term->rc;
}

void
decrement_rc(NilTerm *term)
{
	term->rc -=1;
}

void
increment_rc(NilTerm *term)
{
	term->rc += 1;
}

NilType const *
nil_type(void const *t)
{
	NilTerm const *term = t;
	if (term != NULL) {
		return term->type;
	} else {
		return NULL;
	}
}

void **
nil_children(void const *t, size_t *num_children)
{
	NilTerm const *term = t;
	if (term == NULL) {
		*num_children = 0;
		return NULL;
	}
	NilType const *type = nil_type(term);
	NilChildrenFunc children = nil_type_children(type);
	return children(term, num_children);
}

bool
nil_type_p(NilType const *type, void const *t)
{
	NilTerm const *term = t;
	return type == nil_type(term);
}

NilFreeFunc
nil_type_free(NilType const *type)
{
	return type->free;
}

size_t
nil_type_size(NilType const *type)
{
	return type->size;
}

NilChildrenFunc
nil_type_children(NilType const *type)
{
	return type->children;
}

NilTypeFlags
nil_type_flags(NilType const *type)
{
	return type->flags;
}

void
nil_free_generic(void *term)
{
	free(term);
}

void
nil_free_static(void *term)
{
	(void) term;
}

void **
nil_no_children(void const *term, size_t *num_children)
{
	(void) term;
	*num_children = 0;
	return NULL;
}
