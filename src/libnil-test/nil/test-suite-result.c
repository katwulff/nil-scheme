/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdlib.h>

#include <nil/test.h>

NilTestResultFunctions const nil_test_suite_result_functions = {
	.report       =
	    (NilTestReportFunc) nil_test_suite_result_report,
};

NilTestSuiteResult *
nil_test_suite_result_make(NilTestSuite *test_suite)
{
	NilTestSuiteResult *result = calloc(1, sizeof(*result));
	result->result = (NilTestResult) {
		.functions    = &nil_test_suite_result_functions,
	};
	result->test_suite = test_suite;
	result->length = test_suite->length;
	result->results =
	    calloc(result->length, sizeof(*result->results));
	return result;
}

int
nil_test_suite_result_report(NilTestSuiteResult *result)
{
	int num_errors = 0;
	for (size_t i = 0; i < result->length; ++i) {
		NilTestResult *test_result = result->results[i];
		num_errors += nil_test_result_report(test_result);
	}
	return num_errors;
}
