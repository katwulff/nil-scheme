.POSIX:

all: build

.PHONY: all build check clean
.SUFFIXES:

build: nil-scheme

CFLAGS = -g -O
LDFLAGS =
INCLUDE_DIRS = src/libnil src/libnil-test
CPPFLAGS = $(INCLUDE_DIRS:%=-I%)

deps/generate-dependencies: deps/generate-dependencies.c
	$(CC) -o $@ $?

include src/libnil/libnil.mk
include src/libnil-test/libnil-test.mk
include src/nil-scheme/nil-scheme.mk
include test/test.mk

clean:
	@rm -f libnil.a
	@rm -f $(LIBNIL_A_OBJS)
	@rm -f libnil-test.a
	@rm -f $(LIBNIL_TEST_A_OBJS)
	@rm -f test/libnil-check
	@rm -f $(LIBNIL_CHECK_OBJS)
	@rm -f nil-scheme
	@rm -f $(NIL_SCHEME_OBJS)
