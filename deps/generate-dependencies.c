/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 200809L

#include <regex.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Strings {
	char const    **strings;
	size_t          length;
	size_t          capacity;
} Strings;

static bool     has_final_slash_p(char const *);
static Strings *include_lines(char const *);
static char    *include_statement(char const *);
static void     locate_includes(Strings const *, char const *);
static char    *make_path(char const *, char const *);
static bool     path_exists_p(char const *);

static Strings *make_strings();
static void     strings_add(Strings *, char const *);
static void     strings_free(Strings *);

static FILE    *output_file;

int
main(int argc, char *argv[])
{
	if (argc < 4) {
		fprintf(stderr,
		    "Usage: %s D_FILE C_FILE [INCLUDE_DIR]\n",
		    argv[0]);
		return EXIT_FAILURE;
	}

	char const *input_d = argv[1];
	char const *input_c = argv[2];
	char const *input_o = argv[3];
	output_file = fopen(input_d, "w");
	if (output_file == NULL) {
		fprintf(stderr, "could not open %s\n", input_d);
		return EXIT_FAILURE;
	}
	fprintf(output_file, "%s: %s deps/generate-dependencies\n"
	    "\t./deps/generate-dependencies %s %s %s $(INCLUDE_DIRS)\n\n",
	    input_d, input_c, input_d, input_c, input_o);
	fprintf(output_file, "%s: %s",
	        input_o, input_c);

	Strings *included_files = include_lines(input_c);
	for (int i = 3; i < argc; ++i) {
		char const *include_dir = argv[i];
		locate_includes(included_files, include_dir);
	}
	fprintf(output_file,
	        "\n\t$(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ %s\n",
	        input_c);
	strings_free(included_files);
	fclose(output_file);
	return EXIT_SUCCESS;
}

Strings *
include_lines(char const *input_file)
{
	FILE *file = fopen(input_file, "r");
	if (file == NULL) return NULL;

	Strings *strings = make_strings();
	char *line;
	size_t line_length;
	while (!feof(file)) {
		line = NULL;
		line_length = 0;
		getline(&line, &line_length, file);
		char const *include = include_statement(line);
		if (include != NULL) {
			strings_add(strings, include);
		}
		free(line);
	}
	fclose(file);
	return strings;
}

Strings *
make_strings()
{
	Strings *strings = malloc(sizeof(*strings));
	strings->strings = calloc(8, sizeof(*strings->strings));
	strings->capacity = 8;
	strings->length = 0;
	return strings;
}

void
strings_add(Strings *strings, char const *string)
{
	strings->strings[strings->length] = string;
	strings->length += 1;
	if (strings->length == strings->capacity) {
		size_t new_capacity = strings->capacity * 2;
		strings->strings =
		    realloc(strings->strings,
		            sizeof(*strings->strings) * new_capacity);
		strings->capacity = new_capacity;
	}
}

void
strings_free(Strings *strings)
{
	free(strings->strings);
	free(strings);
}

char *
include_statement(char const *line)
{
	static regex_t include_regex;
	static bool regex_initialised = false;

	if (!regex_initialised) {
		char const *regex =
		    "^#include[ \t]+[<\"](.+)[>\"]";
		regcomp(&include_regex, regex, REG_EXTENDED);
	}
	regmatch_t include_match[2];
	int res = regexec(&include_regex, line, 2, include_match, 0);
	if (res == 0) {
		size_t length = include_match[1].rm_eo -
		                include_match[1].rm_so;
		char *match = calloc(length + 1, sizeof(*match));
		strncpy(match, &line[include_match[1].rm_so], length);
		return match;
	} else {
		return NULL;
	}
}

void
locate_includes(Strings const *strings, char const *dir)
{
	for (size_t i = 0; i < strings->length; ++i) {
		char *path = make_path(dir, strings->strings[i]);
		if (path_exists_p(path)) {
			fprintf(output_file, " %s", path);
		}
	}
}

char *
make_path(char const *dir, char const *file)
{
	size_t dir_length = strlen(dir);
	size_t file_length = strlen(file);
	char *path = calloc(dir_length + file_length + 2,
	                    sizeof(*path));
	memcpy(path, dir, dir_length);
	if (has_final_slash_p(dir)) {
		memcpy(&path[dir_length], file, file_length);
	} else {
		path[dir_length] = '/';
		memcpy(&path[dir_length+1], file, file_length);
	}
	return path;
}

bool
path_exists_p(char const *path)
{
	FILE *file = fopen(path, "r");
	if (file == NULL) {
		return false;
	} else {
		fclose(file);
		return true;
	}
}

bool
has_final_slash_p(char const *path)
{
	size_t path_length = strlen(path);
	return path[path_length - 1] == '/';
}
