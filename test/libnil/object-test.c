/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <nil/test.h>
#include <nil/object.h>
#include "object-test.h"

typedef struct ComplexCycleObject ComplexCycleObject;
typedef struct SimpleCycleObject SimpleCycleObject;
typedef struct SimpleObject SimpleObject;
typedef struct OneChildObject OneChildObject;

struct ComplexCycleObject {
	NilTerm         term;
	SimpleCycleObject *simple_cycle_object;
	ComplexCycleObject *complex_cycle_object;
};

struct SimpleCycleObject {
	NilTerm         term;
	SimpleCycleObject *cycle_object;
	SimpleObject      *simple_object;
};

struct SimpleObject {
	NilTerm         term;
};

struct OneChildObject {
	NilTerm         term;
	SimpleObject   *child;
};

static void     free_object(void *);
static void   **complex_cycle_object_children(ComplexCycleObject *,
                                              size_t *);
static ComplexCycleObject *complex_cycle_object_make(ComplexCycleObject *);
static void     complex_cycle_object_set_cdr(ComplexCycleObject *,
                                             ComplexCycleObject *);
static void     complex_cycle_test(void *);
static void     make_free_test(void *);
static void   **one_child_object_children(OneChildObject *, size_t *);
static OneChildObject *one_child_object_make();
static void     one_child_test(void *);
static void   **simple_cycle_children(SimpleCycleObject *, size_t *);
static SimpleCycleObject *simple_cycle_object_make();
static void     simple_cycle_test(void *);
static SimpleObject *simple_object_make();

static size_t   live_objects = 0;

static NilType one_child_object_type = {
	.free         = (NilFreeFunc) free_object,
	.children     = (NilChildrenFunc) one_child_object_children,
	.size         = sizeof(OneChildObject),
	.flags        = nil_type_flags_none,
};

static NilType simple_object_type = {
	.free         = (NilFreeFunc) free_object,
	.children     = nil_no_children,
	.size         = sizeof(SimpleObject),
	.flags        = nil_type_flags_none,
};

static NilType simple_cycle_type = {
	.free         = (NilFreeFunc) free_object,
	.children     = (NilChildrenFunc) simple_cycle_children,
	.size         = sizeof(SimpleCycleObject),
	.flags        = nil_type_flags_can_cycle,
};

static NilType complex_cycle_type = {
	.free         = (NilFreeFunc) free_object,
	.children     = (NilChildrenFunc) complex_cycle_object_children,
	.size         = sizeof(ComplexCycleObject),
	.flags        = nil_type_flags_can_cycle,
};

static NIL_TEST_CASE(make_free_test_case, "make-free-test-case",
                     make_free_test, NULL, NULL);
static NIL_TEST_CASE(one_child_test_case, "one-child-test-case",
                     one_child_test, NULL, NULL);
static NIL_TEST_CASE(simple_cycle_test_case, "simple-cycle-test-case",
                     simple_cycle_test, NULL, NULL);
static NIL_TEST_CASE(complex_cycle_test_case, "complex-cycle-test-case",
                     complex_cycle_test, NULL, NULL);
NIL_TEST_SUITE(object_suite, "object-suite",
               &make_free_test_case,
               &one_child_test_case,
               &simple_cycle_test_case,
               &complex_cycle_test_case);

void
make_free_test(void *o)
{
	(void) o;
	assert(live_objects == 0);
	SimpleObject *object = simple_object_make();
	assert(live_objects == 1);
	nil_free(object);
	assert(live_objects == 0);
}

void
one_child_test(void *o)
{
	(void) o;
	assert(live_objects == 0);
	OneChildObject *object = one_child_object_make();
	assert(live_objects == 2);
	nil_free(object);
	assert(live_objects == 0);
}

void
simple_cycle_test(void *o)
{
	(void) o;
	assert(live_objects == 0);
	SimpleCycleObject *object = simple_cycle_object_make();
	assert(live_objects == 2);
	nil_free(object);
	assert(live_objects == 0);
}

void
complex_cycle_test(void *o)
{
	(void) o;
	assert(live_objects == 0);
	ComplexCycleObject *object_a = complex_cycle_object_make(NULL);
	assert(live_objects == 3);
	ComplexCycleObject *object_b =
	    complex_cycle_object_make(object_a);
	assert(live_objects == 6);
	complex_cycle_object_set_cdr(object_a, object_b);
	assert(live_objects == 6);
	nil_free(object_a);
	assert(live_objects == 6);
	nil_free(object_b);
	assert(live_objects == 0);
}

SimpleObject *
simple_object_make()
{
	live_objects += 1;
	return nil_make(&simple_object_type);
}

void
free_object(void *object)
{
	live_objects -=1;
	nil_free_generic(object);
}

void **
one_child_object_children(OneChildObject *term, size_t *num_children)
{
	void **child = malloc(sizeof(*child));
	child[0] = term->child;
	*num_children = 1;
	return child;
}

OneChildObject *
one_child_object_make()
{
	OneChildObject *object = nil_make(&one_child_object_type);
	object->child = simple_object_make();
	live_objects += 1;
	return object;
}

void **
simple_cycle_children(SimpleCycleObject *object, size_t *num_children)
{
	void **children = calloc(2, sizeof(*children));
	children[0] = object->cycle_object;
	children[1] = object->simple_object;
	*num_children = 2;
	return children;
}

SimpleCycleObject *
simple_cycle_object_make()
{
	SimpleCycleObject *object = nil_make(&simple_cycle_type);
	object->simple_object = simple_object_make();
	object->cycle_object = nil_keep(object);
	live_objects += 1;
	return object;
}

void **
complex_cycle_object_children(ComplexCycleObject *object, size_t *num_children)
{
	void **children = calloc(2, sizeof(*children));
	children[0] = object->simple_cycle_object;
	children[1] = object->complex_cycle_object;
	*num_children = 2;
	return children;
}

ComplexCycleObject *
complex_cycle_object_make(ComplexCycleObject *cdr)
{
	ComplexCycleObject *object = nil_make(&complex_cycle_type);
	object->simple_cycle_object = simple_cycle_object_make();
	object->complex_cycle_object = nil_keep(cdr);
	live_objects += 1;
	return object;
}

void
complex_cycle_object_set_cdr(ComplexCycleObject *object,
                             ComplexCycleObject *cdr)
{
	nil_free(object->complex_cycle_object);
	object->complex_cycle_object = nil_keep(cdr);
}
