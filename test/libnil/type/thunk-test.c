/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/function.h>
#include <nil/type/null.h>
#include <nil/type/thunk.h>
#include "thunk-test.h"

typedef struct TestData {
	NilThunk       *thunk;
	NilFunction    *function;
} TestData;

static void     make_free_test(TestData *);
static void     thunk_p_test(TestData *);
static void     thunk_evaluate_test(TestData *);
static void     thunk_evaluated_p_test(TestData *);
static void     setup(TestData *);
static NilNull *test_thunk(void *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_test_case,
                     "make-free-test-case",
                     (NilTestFunc) make_free_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(thunk_p_test_case,
                     "thunk?-test-case",
                     (NilTestFunc) thunk_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(thunk_evaluate_test_case,
                     "thunk-evaluate-test-case",
                     (NilTestFunc) thunk_evaluate_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(thunk_evaluated_p_test_case,
                     "thunk-evaluated?-test-case",
                     (NilTestFunc) thunk_evaluated_p_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_thunk_suite, "thunk-suite",
               &make_free_test_case,
               &thunk_p_test_case,
               &thunk_evaluate_test_case,
               &thunk_evaluated_p_test_case);

void
make_free_test(TestData *test_data)
{
	nil_free(test_data->thunk);
}

void
thunk_p_test(TestData *test_data)
{
	assert(nil_thunk_p(test_data->thunk));
}

void
thunk_evaluate_test(TestData *test_data)
{
	void *data = nil_thunk_evaluate(test_data->thunk);
	assert(nil_null_p(data));
}

void
thunk_evaluated_p_test(TestData *test_data)
{
	assert(!nil_thunk_evaluated_p(test_data->thunk));
	NilNull *null =
	    nil_thunk_evaluate(test_data->thunk);
	assert(nil_thunk_evaluated_p(test_data->thunk));
	nil_free(null);
}

void
setup(TestData *test_data)
{
	test_data->function =
	    nil_function_make(NULL, (NilFunc) test_thunk);
	test_data->thunk = nil_thunk_make(test_data->function);
	nil_free(test_data->function);
}

NilNull *
test_thunk(void *env)
{
	(void) env;
	return nil_null_make();
}
