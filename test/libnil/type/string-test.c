/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/string.h>
#include "string-test.h"

typedef struct TestData {
	char const     *raw_string;
	size_t          length;
	NilString      *string;
} TestData;

static void     make_free_test(TestData *);
static void     string_p_test(TestData *);
static void     string_value_test(TestData *);
static void     string_length_test(TestData *);
static void     setup(TestData *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_test_case,
                     "make-free-test-case",
                     (NilTestFunc) make_free_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(string_p_test_case,
                     "string?-test-case",
                     (NilTestFunc) string_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(string_value_test_case,
                     "string-value-test-case",
                     (NilTestFunc) string_value_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(string_length_test_case,
                     "string-length-test-case",
                     (NilTestFunc) string_length_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_string_suite, "string-suite",
               &make_free_test_case,
               &string_p_test_case,
               &string_value_test_case,
               &string_length_test_case);

void
make_free_test(TestData *test_data)
{
	nil_free(test_data->string);
}

void
string_p_test(TestData *test_data)
{
	assert(nil_string_p(test_data->string));
}

void
string_value_test(TestData *test_data)
{
	assert(nil_string_value(test_data->string) ==
	       test_data->raw_string);
}

void
string_length_test(TestData *test_data)
{
	assert(nil_string_length(test_data->string) ==
	       test_data->length);
}

void
setup(TestData *test_data)
{
	test_data->raw_string = "Hello, World!";
	test_data->length = strlen(test_data->raw_string);
	test_data->string =
	    nil_string_make_static(test_data->raw_string);
}
