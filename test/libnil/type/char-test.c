/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/char.h>
#include "char-test.h"

#define TEST_VALUE 'a'

typedef struct TestData {
	NilChar        *char_;
} TestData;

static void     make_free_test(TestData *);
static void     char_p_test(TestData *);
static void     char_value_test(TestData *);
static void     setup(TestData *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_test_case,
                     "make-free-test-case",
                     (NilTestFunc) make_free_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(char_p_test_case,
                     "char?-test-case",
                     (NilTestFunc) char_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(char_value_test_case,
                     "char-value-case",
                     (NilTestFunc) char_value_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_char_suite, "char-suite",
               &make_free_test_case,
               &char_p_test_case,
               &char_value_test_case);

void
make_free_test(TestData *test_data)
{
	nil_free(test_data->char_);
}

void
char_p_test(TestData *test_data)
{
	assert(nil_char_p(test_data->char_));
}

void
char_value_test(TestData *test_data)
{
	assert(nil_char_value(test_data->char_) == TEST_VALUE);
}

void
setup(TestData *test_data)
{
	test_data->char_ = nil_char_make(TEST_VALUE);
}
