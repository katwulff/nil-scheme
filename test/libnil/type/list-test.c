/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/bool.h>
#include <nil/type/eof-object.h>
#include <nil/type/list.h>
#include "list-test.h"

typedef struct TestData {
	NilList        *empty;
	NilList        *list_a;
	NilList        *list_b;
	NilBool        *false_;
	NilBool        *true_;
} TestData;

static void     make_free_test(TestData *);
static void     list_p_test(TestData *);
static void     list_car_test(TestData *);
static void     list_cdr_test(TestData *);
static void     list_empty_p_test(TestData *);
static void     setup(TestData *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_test_case,
                     "make-free-test-case",
                     (NilTestFunc) make_free_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(list_p_test_case,
                     "list?-test-case",
                     (NilTestFunc) list_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(list_car_test_case,
                     "list-car-test-case",
                     (NilTestFunc) list_car_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(list_cdr_test_case,
                     "list-cdr-test-case",
                     (NilTestFunc) list_cdr_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(list_empty_p_test_case,
                     "list-empty?-test-case",
                     (NilTestFunc) list_empty_p_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_list_suite, "list-suite",
               &make_free_test_case,
               &list_p_test_case,
               &list_car_test_case,
               &list_cdr_test_case,
               &list_empty_p_test_case);

void
make_free_test(TestData *test_data)
{
	nil_free(test_data->list_b);
}

void
list_p_test(TestData *test_data)
{
	assert(nil_list_p(test_data->list_a));
	assert(nil_list_p(test_data->list_b));
	assert(nil_list_p(test_data->empty));
}

void
list_car_test(TestData *test_data)
{
	assert(nil_list_car(test_data->list_a) == test_data->false_);
	assert(nil_list_car(test_data->list_b) == test_data->true_);
}

void
list_cdr_test(TestData *test_data)
{
	assert(nil_list_cdr(test_data->list_a) == test_data->empty);
	assert(nil_list_cdr(test_data->list_b) == test_data->list_a);
}

void
list_empty_p_test(TestData *test_data)
{
	assert(!nil_list_empty_p(test_data->list_a));
	assert(!nil_list_empty_p(test_data->list_b));
	assert(nil_list_empty_p(test_data->empty));
}

void
setup(TestData *test_data)
{
	test_data->false_ = nil_bool_make(false);
	test_data->true_ = nil_bool_make(true);
	test_data->empty = nil_list_empty();
	test_data->list_a =
	    nil_list_cons(test_data->false_, test_data->empty);
	test_data->list_b =
	    nil_list_cons(test_data->true_, test_data->list_a);
	nil_free(test_data->false_);
	nil_free(test_data->true_);
	nil_free(test_data->empty);
	nil_free(test_data->list_a);
}
