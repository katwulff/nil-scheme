/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/null.h>
#include "null-test.h"

typedef struct TestData {
	NilNull       *null;
} TestData;

static void     make_free_test(TestData *);
static void     null_p_test(TestData *);
static void     setup(TestData *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_test_case,
                     "make-free-test-case",
                     (NilTestFunc) make_free_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(null_p_test_case,
                     "null?-test-case",
                     (NilTestFunc) null_p_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_null_suite, "null-suite",
               &make_free_test_case,
               &null_p_test_case);

void
make_free_test(TestData *test_data)
{
	nil_free(test_data->null);
}

void
null_p_test(TestData *test_data)
{
	assert(nil_null_p(test_data->null));
}

void
setup(TestData *test_data)
{
	test_data->null = nil_null_make();
}
