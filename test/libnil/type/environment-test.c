/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/symbol.h>
#include <nil/type/environment.h>
#include "environment-test.h"

typedef struct TestData {
	NilSymbol      *symbol_a;
	NilSymbol      *symbol_b;
	NilSymbol      *symbol_a_base;
	NilSymbol      *symbol_b_base;
	NilSymbol      *symbol_b_sub;
	NilEnvironment *base;
	NilEnvironment *sub;
} TestData;

static void     make_free_test(TestData *);
static void     environment_p_test(TestData *);
static void     environment_lookup_test(TestData *);
static void     setup(TestData *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_test_case,
                     "make-free-test-case",
                     (NilTestFunc) make_free_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(environment_p_test_case,
                     "environment?-test-case",
                     (NilTestFunc) environment_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(environment_lookup_test_case,
                     "environment-lookup-test-case",
                     (NilTestFunc) environment_lookup_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_environment_suite, "environment-suite",
               &make_free_test_case,
               &environment_p_test_case,
               &environment_lookup_test_case);

void
make_free_test(TestData *test_data)
{
	nil_free(test_data->sub);
	nil_free(test_data->base);
}

void
environment_p_test(TestData *test_data)
{
	assert(nil_environment_p(test_data->sub));
	assert(nil_environment_p(test_data->base));
}

void
environment_lookup_test(TestData *test_data)
{
	NilEnvironment *base = test_data->base;
	NilEnvironment *sub = test_data->sub;
	NilSymbol *symbol_a = test_data->symbol_a;
	NilSymbol *symbol_b = test_data->symbol_b;
	NilSymbol *symbol_a_base = test_data->symbol_a_base;
	NilSymbol *symbol_b_base = test_data->symbol_b_base;
	NilSymbol *symbol_b_sub = test_data->symbol_b_sub;
	assert(nil_environment_lookup(base, symbol_a) == symbol_a_base);
	assert(nil_environment_lookup(base, symbol_b) == symbol_b_base);
	assert(nil_environment_lookup(sub, symbol_a) == symbol_a_base);
	assert(nil_environment_lookup(sub, symbol_b) == symbol_b_base);
	nil_environment_define(sub, symbol_b, symbol_b_sub);
	assert(nil_environment_lookup(base, symbol_b) == symbol_b_base);
	assert(nil_environment_lookup(sub, symbol_b) == symbol_b_sub);
}

void
setup(TestData *test_data)
{
	test_data->symbol_a = nil_symbol_make_static("a");
	test_data->symbol_b = nil_symbol_make_static("b");
	test_data->symbol_a_base = nil_symbol_make_static("a-base");
	test_data->symbol_b_base = nil_symbol_make_static("b-base");
	test_data->symbol_b_sub = nil_symbol_make_static("b-sub");
	test_data->base = nil_environment_make(NULL);
	test_data->sub = nil_environment_make(test_data->base);
	nil_environment_define(test_data->base,
	                       test_data->symbol_a,
	                       test_data->symbol_a_base);
	nil_environment_define(test_data->base,
	                       test_data->symbol_b,
	                       test_data->symbol_b_base);
}
