/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/pair.h>
#include <nil/type/null.h>
#include "pair-test.h"

typedef struct TestData {
	NilPair       *pair;
	NilNull       *null_a;
	NilNull       *null_b;
} TestData;

static void     make_free_test(TestData *);
static void     pair_p_test(TestData *);
static void     pair_first_test(TestData *);
static void     pair_second_test(TestData *);
static void     setup(TestData *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_test_case,
                     "make-free-test-case",
                     (NilTestFunc) make_free_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(pair_p_test_case,
                     "pair?-test-case",
                     (NilTestFunc) pair_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(pair_first_test_case,
                     "pair-first-test-case",
                     (NilTestFunc) pair_first_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(pair_second_test_case,
                     "pair-second-test-case",
                     (NilTestFunc) pair_second_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_pair_suite, "pair-suite",
               &make_free_test_case,
               &pair_p_test_case,
               &pair_first_test_case,
               &pair_second_test_case);

void
make_free_test(TestData *test_data)
{
	nil_free(test_data->pair);
}

void
pair_p_test(TestData *test_data)
{
	assert(nil_pair_p(test_data->pair));
}

void
pair_first_test(TestData *test_data)
{
	assert(nil_pair_first(test_data->pair) == test_data->null_a);
}

void
pair_second_test(TestData *test_data)
{
	assert(nil_pair_second(test_data->pair) == test_data->null_b);
}

void
setup(TestData *test_data)
{
	test_data->null_a = nil_null_make();
	test_data->null_b = nil_null_make();
	test_data->pair =
	    nil_pair_make(test_data->null_a, test_data->null_b);
}
